import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The World for our elephant to play in.
 * 
 * @author Mr. Chan
 * @version November 2018
 */
public class MyWorld extends World
{
    /**
     * Constructor for objects of class MyWorld.
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        Elephant elly = new Elephant();
        addObject(elly, 300, 300);
        spawnApple();
    }
    
    public void spawnApple()
    {
        Apple apple = new Apple();
        int x = Greenfoot.getRandomNumber(600);
        int y = 20;
        addObject(apple, x, y);
    }
    
    public void act()
    {
        
    }
}
