import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Our Hero, Elly the Elephant.  Elly likes to eat.
 * 
 * @author Mr. Chan 
 * @version November 2018
 */
public class Elephant extends Actor
{
    public void act() 
    {
        if (Greenfoot.isKeyDown("left")) { move(-3); }
        if (Greenfoot.isKeyDown("right")){ move(3); }
        if (Greenfoot.isKeyDown("up")) { setLocation(getX(), getY() - 3); }
        if (Greenfoot.isKeyDown("down")) { setLocation(getX(), getY() + 3); }
    }    
}
